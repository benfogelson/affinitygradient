import click
import pypet
import pipelines
from parameters import ParameterSet
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)


class RunState(object):
    def __init__(self, sumatra_parameters: ParameterSet, env: pypet.Environment):
        self.sumatra_parameters = sumatra_parameters
        self.env = env


@click.group()
@click.argument('configfile', type=click.File())
@click.pass_context
def cli(ctx: click.Context, configfile: click.File):
    sumatra_parameters = ParameterSet(configfile.name)

    hdf5filename = sumatra_parameters['pypet']['hdf5file']

    try:
        trajectory_name = sumatra_parameters['sumatra_label']
        add_time = False
    except KeyError:
        trajectory_name = 'trajectory'
        add_time = True

    env = pypet.Environment(
        trajectory=trajectory_name,
        add_time=add_time,
        filename=hdf5filename,
        multiproc=sumatra_parameters['pypet']['multiproc'],
        ncores=sumatra_parameters['pypet']['ncores'])

    env.disable_logging()

    traj = env.trajectory   # type: pypet.Trajectory
    traj.f_add_config('configfile', configfile.name)

    ctx.obj = RunState(sumatra_parameters, env)


@cli.command()
@click.pass_obj
def default(obj: RunState):
    obj.env.pipeline(pipelines.pipeline)

    try:
        sumatra_label = obj.sumatra_parameters['sumatra_label']
        click.echo('Finished running. To run analyze script, use')
        click.echo('smt run -m analyze.py {0} {1}'.format(sumatra_label, 'config.yaml'))
    except KeyError:
        pass


@cli.command()
@click.pass_obj
def asymptotic(obj: RunState):
    obj.env.pipeline(pipelines.pipeline_asymptotic)


@cli.command()
@click.pass_obj
def explore2d(obj: RunState):
    obj.env.pipeline(pipelines.pipeline_explore2d)


if __name__ == '__main__':
    cli()

# @click.group()
# @click.option('--debug/--no-debug', default=False)
# @click.pass_context
# def cli(ctx, debug):
#     ctx.obj['DEBUG'] = debug
#
# @cli.command()
# @click.pass_context
# def sync(ctx):
#     click.echo('Debug is %s' % (ctx.obj['DEBUG'] and 'on' or 'off'))
#
# if __name__ == '__main__':
#     cli(obj={})