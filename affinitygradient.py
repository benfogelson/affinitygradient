import click
import numpy as np
import scipy.integrate
# import matplotlib.pyplot as plt


def run():
    pass


class AffinityGradientBVP(object):
    @classmethod
    def create_neumann(cls, alpha, lambdau, lambdav, kappaoutu, kappaoutv, deltav, u0):
        bvp = AffinityGradientBVP(alpha, lambdau, lambdav, kappaoutu, kappaoutv, deltav, u0)
        return bvp

    @classmethod
    def create_dirichlet(cls, alpha, lambdau, lambdav, deltav, u0):
        bvp = AffinityGradientBVP(alpha, lambdau, lambdav, np.inf, np.inf, deltav, u0)
        return bvp

    def __init__(self, alpha, lambdau, lambdav, kappaoutu, kappaoutv, deltav, u0):
        self.alpha = alpha
        self.lambdau = lambdau
        self.lambdav = lambdav
        self.kappaoutu = kappaoutu
        self.kappaoutv = kappaoutv
        self.deltav = deltav
        self.u0 = u0

    def is_dirichlet(self):
        return self.kappaoutu == np.inf or self.kappaoutv == np.inf

    def is_neumann(self):
        return not self.is_dirichlet()

    def func(self, x, y):
        out = np.zeros_like(y)

        alpha = self.alpha
        lambdau = self.lambdau
        lambdav = self.lambdav

        u = y[0, :]
        v = y[1, :]
        ju = y[2, :]
        jv = y[3, :]
        
        out[0, :] = (alpha * jv * lambdau * u + ju * (
                -1 - 2 * u - u ** 2 + (-2 - alpha * lambdav) * v - 2 * u * v - v ** 2)) / (
                1 + (2 + alpha * lambdau) * u + u ** 2 + (2 + alpha * lambdav) * v + 2 * u * v + v ** 2)
        out[1, :] = (alpha * ju * lambdav * v + jv * (
                -1 + (-2 - alpha * lambdau) * u - u ** 2 - 2 * v - 2 * u * v - v ** 2)) / (
                1 + (2 + alpha * lambdau) * u + u ** 2 + (2 + alpha * lambdav) * v + 2 * u * v + v ** 2)

        return out

    def fun_jac(self, x, y):
        n, m = y.shape
        df_dy = np.zeros((n, n, m))

        alpha = self.alpha
        lambdau = self.lambdau
        lambdav = self.lambdav

        u = y[0, :]
        v = y[1, :]
        ju = y[2, :]
        jv = y[3, :]

        # dfu_du
        df_dy[0, 0, :] = (alpha*(ju + jv)*lambdau*(1 - u**2 + (2 + alpha*lambdav)*v + v**2))/(1 + u**2 + (2 + alpha*lambdav)*v + v**2 + u*(2 + alpha*lambdau + 2*v))**2

        # dfu_dv
        df_dy[0, 1, :] = -((alpha*(ju + jv)*lambdau*u*(alpha*lambdav + 2*(1 + u + v)))/(1 + u**2 + (2 + alpha*lambdav)*v + v**2 + u*(2 + alpha*lambdau + 2*v))**2)

        # dfu_dju
        df_dy[0, 2, :] = -((1 + u**2 + (2 + alpha*lambdav)*v + v**2 + 2*u*(1 + v))/(1 + u**2 + (2 + alpha*lambdav)*v + v**2 + u*(2 + alpha*lambdau + 2*v)))

        # dfu_djv
        df_dy[0, 3, :] = (alpha*lambdau*u)/(1 + u**2 + (2 + alpha*lambdav)*v + v**2 + u*(2 + alpha*lambdau + 2*v))

        # dfv_du
        df_dy[1, 0, :] = -((alpha*(ju + jv)*lambdav*v*(alpha*lambdau + 2*(1 + u + v)))/(1 + u**2 + (2 + alpha*lambdav)*v + v**2 + u*(2 + alpha*lambdau + 2*v))**2)

        # dfv_dv
        df_dy[1, 1, :] = (alpha*(ju + jv)*lambdav*(1 + (2 + alpha*lambdau)*u + u**2 - v**2))/(1 + u**2 + (2 + alpha*lambdav)*v + v**2 + u*(2 + alpha*lambdau + 2*v))**2

        # dfv_dju
        df_dy[1, 2, :] = (alpha*lambdav*v)/(1 + u**2 + (2 + alpha*lambdav)*v + v**2 + u*(2 + alpha*lambdau + 2*v))

        # dfv_djv
        df_dy[1, 3, :] = -((u**2 + (1 + v)**2 + u*(2 + alpha*lambdau + 2*v))/(1 + u**2 + (2 + alpha*lambdav)*v + v**2 + u*(2 + alpha*lambdau + 2*v)))

        return df_dy

    def bc(self, ya, yb):
        out = np.zeros(ya.shape)

        lambdau = self.lambdau
        lambdav = self.lambdav
        kappaoutu = self.kappaoutu
        kappaoutv = self.kappaoutv
        deltav = self.deltav
        u0 = self.u0

        ua = ya[0]
        va = ya[1]
        jua = ya[2]
        jva = ya[3]

        ub = yb[0]
        vb = yb[1]
        jub = yb[2]
        jvb = yb[3]

        out[0] = -u0 + ua
        out[3] = deltav*jvb + (jub*lambdav)/lambdau

        if kappaoutu == 'inf':
            out[2] = ub
        else:
            out[2] = jub - kappaoutu*ub

        if kappaoutv == 'inf':
            out[1] = va
        else:
            out[1] = deltav*jva + kappaoutv*va

        return out

    def bc_jac(self, ya, yb):
        n, = ya.shape

        lambdau = self.lambdau
        lambdav = self.lambdav
        kappaoutu = self.kappaoutu
        kappaoutv = self.kappaoutv
        deltav = self.deltav
        u0 = self.u0

        ua = ya[0]
        va = ya[1]
        jua = ya[2]
        jva = ya[3]

        ub = yb[0]
        vb = yb[1]
        jub = yb[2]
        jvb = yb[3]

        if kappaoutu != 'inf' or kappaoutv != 'inf':
            raise RuntimeError('bc_jac only implemented for Dirichlet BCs')

        dbc_dya = np.zeros((n, n))
        dbc_dyb = np.zeros((n, n))

        # dbc_dua
        dbc_dya[0, 0] = 1

        # dbc_dva
        dbc_dya[1, 1] = 1

        # dbc_dub
        dbc_dyb[2, 0] = 1

        # dbc_djub
        dbc_dyb[3, 2] = lambdav/lambdau

        # dbc_djvb
        dbc_dyb[3, 3] = deltav

        return dbc_dya, dbc_dyb


    def exact_unenhanced(self, x):
        lambdau = self.lambdau
        lambdav = self.lambdav
        kappaoutu = self.kappaoutu
        kappaoutv = self.kappaoutv
        deltav = self.deltav
        u0 = self.u0

        y = np.zeros((4, len(x)))

        y[0, :] = u0 - u0*x
        y[1, :] = (lambdav*u0*x)/(deltav*lambdau)
        y[2, :] = u0
        y[3, :] = -((lambdav*u0)/(deltav*lambdau))

        return y


if __name__ == '__main__':
    bvp = AffinityGradientBVP.create_neumann(
        alpha=100,
        lambdau=1.0,
        lambdav=1.0,
        deltav=1.0,
        u0=1.0,
        kappaoutu=10.0, #np.inf,
        kappaoutv=10.0, #np.inf,
    )

    n0 = 10
    x = np.linspace(0, 1, n0)
    y0 = bvp.exact_unenhanced(x)

    result = scipy.integrate.solve_bvp(bvp.func, bvp.bc, x, y0, tol=1e-8)

    xplot = np.linspace(0, 1, 1000)
    yplot = result.sol(xplot)
    y0plot = bvp.exact_unenhanced(xplot)
    plt.plot(xplot, yplot[0, :])
    plt.plot(xplot, y0plot[0, :])
    plt.show()

    ju = yplot[2, 0]
    ju0 = y0plot[2, 0]

    print(ju)
    print(ju0)
    print(ju/ju0)

