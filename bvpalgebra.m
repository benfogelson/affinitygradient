(* ::Package:: *)

fluxformula[f_]=Subscript[j, f][x]==-f'[x]-\[Alpha] Subscript[\[Lambda], f] f[x] (u'[x]+v'[x])/(1+u[x]+v[x])^2


fluxformula[u]


fluxformula[v]/.v->w


odeSystem = Simplify[Solve[
{fluxformula[u],
fluxformula[v],D[Subscript[j, u][x],x]==0,D[Subscript[j, v][x],x]==0},
{u'[x], v'[x], Subscript[j, u]'[x], Subscript[j, v]'[x]}][[1]]/.Rule->Equal]


simplifiedODESystem = Map[#[[1]]==Collect[Expand[Numerator[#[[2]]]],{Subscript[j, u][x],Subscript[j, v][x]},Collect[#,{u[x]v[x],u[x],v[x]}]&]/Collect[Expand[Denominator[#[[2]]]],{u[x]v[x],u[x],v[x]}]&,odeSystem]


bcs = {u[0]==Subscript[u, 0], Subscript[\[Delta], v]Subscript[j, v][0]==-





\!\(\*SubsuperscriptBox[\(\[Kappa]\), \(v\), \(out\)]\)v[0],Subscript[j, u][1]==\!\(
\*SubsuperscriptBox[\(\[Kappa]\), \(u\), \(out\)] \(u[1]\)\),Subscript[\[Delta], v]Subscript[j, v][1]==-(Subscript[\[Lambda], v]/Subscript[\[Lambda], u])Subscript[j, u][1]}


pythonconversions={
\[Alpha]->alpha,
Subscript[\[Lambda], u]->lambdau,
Subscript[\[Lambda], v]->lambdav,
Subscript[j, u][x]->ju,
Subscript[j, v][x]->jv,
u[x]->u,
v[x]->v,
Subscript[\[Delta], v]->deltav,
Subscript[u, 0]->u0,
u[0]->ua,
u[1]->ub,
v[0]->va,
v[1]->vb,
Subscript[j, u][0]->jua,
Subscript[j, u][1]->jub,
Subscript[j, v][0]->jva,
Subscript[j, v][1]->jvb,
\!\(
\*SubsuperscriptBox[\(\[Kappa]\), \(v\), \(out\)] -> kappaoutv\),
\!\(
\*SubsuperscriptBox[\(\[Kappa]\), \(u\), \(out\)] -> kappaoutu\),
u'[x]->uprime,
v'[x]->vprime,
Subscript[j, u]'[x]->juprime,
Subscript[j, v]'[x]->jvprime,
ju[x]->ju,
jv[x]->jv}


fortranODEs = Map[#[[1]]==FortranForm[#[[2]]]&,simplifiedODESystem//.pythonconversions]


fortranBCs = Map[FortranForm[Simplify[#[[1]]-#[[2]]]]&,bcs//.pythonconversions]


pw = PageWidth/.Options[$Output];
SetOptions[$Output, PageWidth->Infinity];
fortranODEs
fortranBCs
SetOptions[$Output, PageWidth->pw];





dsolveconversions = {Subscript[j, u]->ju,Subscript[j, v]->jv,Subscript[\[Lambda], u]->lambdau,Subscript[\[Lambda], v]->lambdav,\!\(
\*SubsuperscriptBox[\(\[Kappa]\), \(v\), \(out\)] -> kappaoutv\),\!\(
\*SubsuperscriptBox[\(\[Kappa]\), \(u\), \(out\)] -> kappaoutu\),Subscript[\[Delta], v]->deltav,Subscript[u, 0]->u0}


fortranexactsolveneumann=Map[#[[1]]==FortranForm[#[[2]]]&,FullSimplify[DSolve[Simplify[Flatten[{odeSystem,bcs}/.\[Alpha]->0]]/.dsolveconversions,{u[x],v[x],ju[x],jv[x]},x]][[1]]/.pythonconversions]


bcsdirichlet={u[0]==Subscript[u, 0], Subscript[\[Delta], v] v[0]==0, u[1]==0,Subscript[\[Delta], v] Subscript[j, v][1]==-((Subscript[\[Lambda], v] Subscript[j, u][1])/Subscript[\[Lambda], u])}


fortranBCsdirichlet=Map[FortranForm[Simplify[#[[1]]-#[[2]]]]&,bcsdirichlet//.pythonconversions]


Map[#[[1]]==FortranForm[#[[2]]]&,FullSimplify[DSolve[Simplify[Flatten[{odeSystem,bcsdirichlet}/.\[Alpha]->0]]/.dsolveconversions,{u[x],v[x],ju[x],jv[x]},x]][[1]]/.pythonconversions]


odesystemoperatorjacobianform=Map[#[[2]]&,simplifiedODESystem]/.u[x]->uu/.v[x]->vv/.Subscript[j, u][x]->ju/.Subscript[j, v][x]->jv


pythonJac[expr_,var_]:=FortranForm[Simplify[D[expr,var]/.{uu->u[x],vv->v[x],ju->Subscript[j, u][x],jv->Subscript[j, v][x]}]/.pythonconversions]


dudu=pythonJac[odesystemoperatorjacobianform[[1]],uu]


dudv=pythonJac[odesystemoperatorjacobianform[[1]],vv]


dudju=pythonJac[odesystemoperatorjacobianform[[1]],ju]


dudjv=pythonJac[odesystemoperatorjacobianform[[1]],jv]


dvdu=pythonJac[odesystemoperatorjacobianform[[2]],uu]


dvdv=pythonJac[odesystemoperatorjacobianform[[2]],vv]


dvdju=pythonJac[odesystemoperatorjacobianform[[2]],ju]


dvdv=pythonJac[odesystemoperatorjacobianform[[2]],jv]
