import pypet
import pandas as pd
import numpy as np
import scipy.integrate
import scipy.optimize
from parameters import ParameterSet

from affinitygradient import AffinityGradientBVP


def add_dict_to_parameter_group(param_dict: dict, group: pypet.ParameterGroup):
    for key, val in param_dict.items():
        if type(val) is dict:
            subgroup = group.f_add_parameter_group(key)
            add_dict_to_parameter_group(val, subgroup)
        else:
            group.f_add_parameter(key, val)


def add_parameters(traj: pypet.Trajectory):
    configfilename = traj.config.configfile
    sumatra_dict = ParameterSet(configfilename).as_dict()

    add_dict_to_parameter_group(sumatra_dict, traj)


def add_exploration(traj: pypet.Trajectory):
    param = traj.exploration.param

    spacing = traj.exploration.spacing
    if spacing == 'linear':
        spacing_func = np.linspace
    elif spacing == 'log':
        spacing_func = np.logspace
    else:
        raise AttributeError('Unrecognized spacing type exploration.spacing.{0}'.format(spacing))

    traj.f_explore({param: spacing_func(traj.min, traj.max, traj.num).tolist()})


def pipeline(traj: pypet.Trajectory):
    add_parameters(traj)
    add_exploration(traj)

    run_args = ()
    run_kwargs = {}

    postprocess_args = ()
    postprocess_kwargs = {}

    return (run, run_args, run_kwargs), (postprocess, postprocess_args, postprocess_kwargs)


def run(traj: pypet.Trajectory):
    bvp = AffinityGradientBVP(
        alpha = traj.model.alpha,
        lambdau = traj.model.lambdau,
        lambdav = traj.model.lambdav,
        kappaoutu = traj.model.kappaoutu,
        kappaoutv = traj.model.kappaoutv,
        deltav = traj.model.deltav,
        u0 = traj.model.u0
    )

    # For large parameter values, convergence of the bvp solver is sensitive to the number of nodes in the
    # initial guess.
    starting_nodes = [5, 10, 20, 50, 100]

    for n in starting_nodes:
        x0 = np.linspace(0, 1, n)
        y0 = bvp.exact_unenhanced(x0)

        result = scipy.integrate.solve_bvp(fun=bvp.func,
                                           bc=bvp.bc,
                                           x=x0,
                                           y=y0,
                                           tol=float(traj.solver.tol),
                                           verbose=int(traj.solver.verbose),
                                           max_nodes=int(traj.solver.max_nodes))#,
                                           # fun_jac=bvp.fun_jac,
                                           # bc_jac=bvp.bc_jac)

        if result.success:
            break

    if not result.success:
        msg = 'BVP solver failed. Termination reason: {0}'.format(result.message)
        msg += '\n\talpha={0}\n\tlambdau={1}\n\tlambdav={2}'
        msg += '\n\tdeltav={3}\n\tu0={4}'
        msg = msg.format(traj.model.alpha,
                         traj.model.lambdau,
                         traj.model.lambdav,
                         traj.model.deltav,
                         traj.model.u0)
        print(msg)
        # raise RuntimeError(msg)

    x = np.linspace(0, 1, 100)
    y = result.sol(x)
    u = y[0, :]
    v = y[1, :]
    ju = y[2, 0]
    jv = y[3, 0]

    ju_unenhanced = y0[2, 0]
    enhancement = ju/ju_unenhanced

    traj.f_add_result('x', x)
    traj.f_add_result('u', u)
    traj.f_add_result('v', v)
    traj.f_add_result('ju', ju)
    traj.f_add_result('jv', jv)
    traj.f_add_result('ju_unenhanced', ju_unenhanced)
    traj.f_add_result('enhancement', enhancement)

    return ju_unenhanced, ju, enhancement, result.success


def postprocess(traj: pypet.Trajectory, result_list: list):
    runs, results = zip(*result_list)

    param = traj.exploration.param
    param_range = traj.f_get(param).f_get_range()

    ju_unenhanced, ju, enhancement, success = zip(*results)
    df = pd.DataFrame({param: param_range,
                       'ju_unenhanced': ju_unenhanced,
                       'ju': ju,
                       'enhancement': enhancement,
                       'success': success},
                      index=runs)

    traj.f_add_result('summary.flux_enhancement', flux_frame=df)


def pipeline_asymptotic(traj: pypet.Trajectory):
    add_parameters(traj)
    param = traj.exploration.param
    assert param != 'alpha'

    add_exploration(traj)

    run_args = ()
    run_kwargs = {}

    postprocess_args = ()
    postprocess_kwargs = {}

    return (run_asymptotic, run_args, run_kwargs), (postprocess_asymptotic, postprocess_args, postprocess_kwargs)


def pipeline_explore2d(traj: pypet.Trajectory):
    add_parameters(traj)

    explore_dict = {}

    for param in [traj.exploration2d.param1, traj.exploration2d.param2]:
        if param.spacing == 'linear':
            param_range =  np.linspace(param.min, param.max, param.num)
        elif param.spacing == 'log':
            param_range = np.logspace(param.min, param.max, param.num)
        else:
            raise ValueError('Unrecognized parameter spacing "{0}"'.format(param.spacing))

        param_range = param_range.tolist()
        explore_dict[param.name] = param_range

    traj.f_explore(pypet.cartesian_product(explore_dict))

    run_args = ()
    run_kwargs = {}

    postprocess_args = ()
    postprocess_kwargs = {}

    return (run, run_args, run_kwargs), (postprocess_explore2d, postprocess_args, postprocess_kwargs)


def run_asymptotic(traj: pypet.Trajectory):
    def enhancement(alpha):
        bvp = AffinityGradientBVP(
        alpha = alpha,
        lambdau = traj.model.lambdau,
        lambdav = traj.model.lambdav,
        kappaoutu = traj.model.kappaoutu,
        kappaoutv = traj.model.kappaoutv,
        deltav = traj.model.deltav,
        u0 = traj.model.u0
        )

        x0 = np.linspace(0, 1, 10)
        y0 = bvp.exact_unenhanced(x0)

        result = scipy.integrate.solve_bvp(bvp.func, bvp.bc, x0, y0, tol=float(traj.solver.tol))

        x = np.linspace(0, 1, 100)
        y = result.sol(x)
        u = y[0, :]
        v = y[1, :]
        ju = y[2, 0]
        jv = y[3, 0]

        ju_unenhanced = y0[2, 0]
        enhancement = ju/ju_unenhanced

        return enhancement

    alpha0 = float(traj.asymptotic.alpha0)
    alpha_step = float(traj.asymptotic.alpha_step)
    alpha_tol = float(traj.asymptotic.alpha_tol)
    max_steps = int(traj.asymptotic.max_steps)


    converged = False
    e0 = enhancement(alpha0)
    for k in range(max_steps):
        alpha = alpha0 + alpha_step
        e = enhancement(alpha)
        if np.abs(e - e0) < alpha_tol:
            converged = True
            break
        e0 = e
        alpha0 = alpha

    asymptotic_result = traj.f_add_result_group('asymptotic')
    asymptotic_result.f_add_result('converged', converged)
    asymptotic_result.f_add_result('steps', k)
    asymptotic_result.f_add_result('enhancement', e)
    asymptotic_result.f_add_result('alpha', alpha)

    func_half = lambda alpha: enhancement(alpha) - e/2
    func_99 = lambda alpha: enhancement(alpha) - 0.99*e

    if converged:
        if k == 0:
            alpha_guess = 0
        else:
            alpha_guess = alpha/2
    else:
        alpha_guess = alpha

    try:
        alpha_half = scipy.optimize.newton(func_half, alpha_guess)
    except RuntimeError:
        alpha_half = -1
    try:
        alpha_99 = scipy.optimize.newton(func_99, alpha_guess)
    except RuntimeError:
        alpha_99 = -1


    return alpha, e, k, converged, alpha_half, alpha_99


def postprocess_asymptotic(traj: pypet.Trajectory, result_list: list):
    runs, results = zip(*result_list)

    param = traj.exploration.param
    param_range = traj.f_get(param).f_get_range()

    alpha, enhancement, k, converged, alpha_half, alpha_99 = zip(*results)

    df = pd.DataFrame({
        param: param_range,
        'alpha': alpha,
        'enhancement': enhancement,
        'step': k,
        'converged': converged,
        'alpha_half': alpha_half,
        'alpha_99': alpha_99
    })

    traj.f_add_result('summary.asymptoticframe', df=df)


def postprocess_explore2d(traj: pypet.Trajectory, result_list: list):
    runs, results = zip(*result_list)

    ju_unenhanced, ju, enhancement, success = zip(*results)

    param1_name = traj.exploration2d.param1.name
    param2_name = traj.exploration2d.param2.name

    p1 = traj.model.f_get(param1_name).f_get_range()
    p2 = traj.model.f_get(param2_name).f_get_range()

    traj.f_add_result('summary.explore2d', p1=p1, p2=p2, e=enhancement, success=success)
