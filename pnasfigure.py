from figures.pnascommon import *
from figures import alphadependence, cytoplasmdependence, vrates


@click.group()
@click.pass_context
@click.argument('configfile', type=click.File())
def cli(ctx: click.Context, configfile: click.File):
    ctx.obj = SumatraContext(configfile)


cli.add_command(alphadependence.main, name='alphadependence')
cli.add_command(cytoplasmdependence.main, name='cytoplasmdependence')
cli.add_command(vrates.main, name='vrates')


if __name__ == '__main__':
    cli()
