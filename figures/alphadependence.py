from figures.pnascommon import *


@click.command()
@click.pass_obj
def main(obj: SumatraContext):
    sumatra_parameters = obj.sumatra_parameters
    data_store = obj.data_store
    proj = obj.proj

    name = 'alphadependence'

    fig_params = sumatra_parameters[name]

    tol = float(fig_params['solver']['tol'])

    fig_file_name = '{0}.pdf'.format(name)
    fig_file_path = os.path.join(data_store, fig_file_name)

    fig = plt.figure()
    fig.set_size_inches((linewidth_inches, linewidth_inches))
    fig.set_tight_layout(True)

    gs = gridspec.GridSpec(2, 1)

    gs.set_height_ratios([1.8, 1])

    axuv = fig.add_subplot(gs[0, 0])
    # axv = fig.add_subplot(gs[1, 0], sharex=axu, sharey=axu)

    # Steady state u and v plots

    make_plot(fig_params['sim0']['model'],
              axuv,
              axuv,
              tol,
              fig_params['sim0']['style'])

    make_plot(fig_params['sim1']['model'],
              axuv,
              axuv,
              tol,
              fig_params['sim1']['style'])

    make_plot(fig_params['sim2']['model'],
              axuv,
              axuv,
              tol,
              fig_params['sim2']['style'])

    make_plot(fig_params['sim3']['model'],
              axuv,
              axuv,
              tol,
              fig_params['sim3']['style'])

    axuv.set_xlabel(r'{\captionfont \(x\) (axial position along pore)}')

    axuv.set_title(r'{\captiontitlefont Density profiles of \(u\) (\textcolor{C0}{blue}) and \(v\) (\textcolor{C1}{orange})}')

    handles = []
    for color in ['C0', 'C1']:
        for axname in ['sim0', 'sim1', 'sim2', 'sim3']:
            label = r'' if color == 'C0' else r'\(\alpha = {0:.0f}\)'
            line = matplotlib.lines.Line2D([], [], color=color,
                                           label=label.format(fig_params[axname]['model']['alpha']),
                                           linestyle=fig_params[axname]['style']['linestyle'])
            handles.append(line)

    plt.legend(ncol=2, handles=handles, columnspacing=0, handlelength=1.9, loc='middle left')
    # plt.legend(ncol=2)

    # Enhancement scaling with alpha plot

    axscaling = fig.add_subplot(gs[1, 0])
    scaling_label = fig_params['alpha_scale_ax']['record_label']
    rec = proj.get_record(scaling_label) # type: sumatra.records.Record

    traj = pypet.Trajectory(name=scaling_label, add_time=False)
    hdf5filename = rec.parameters['pypet']['hdf5file']
    traj.f_load(load_parameters=2, load_derived_parameters=0, load_results=0, load_other_data=0, filename=hdf5filename)
    traj.v_auto_load = True

    flux_frame = traj.results.summary.flux_enhancement.flux_frame   # type: pd.DataFrame

    axscaling.plot(flux_frame.alpha, flux_frame.enhancement, color='0.35')
    ylim0 = axscaling.get_ylim()
    axscaling.set_ylim(0, ylim0[1])
    axscaling.yaxis.set_ticks([1, 3, 5])

    axscaling.set_xlabel(r'{\captionfont \(\alpha\) (tether density scale)}')
    axscaling.set_ylabel(r'{\captionfont $J_u/J_u^0$}')
    axscaling.set_title(r'{\captiontitlefont Flux enhancement}')

    add_letter(axuv, 'a', -.1, 1)
    add_letter(axscaling, 'b', -.1, 1)

    fig.savefig(fig_file_path)

    copy_to_recent_fig_directory(sumatra_parameters, fig_file_path)


def make_plot(model: dict, axu: plt.Axes, axv: plt.Axes, tol: float = 1e-8, style={}):
    x, u, v = run_solver(model, tol)
    plot_concentrations(x, u, v, axu, axv, style)


def run_solver(model_params, tol):
    bvp = AffinityGradientBVP(**model_params)

    x0 = np.linspace(0, 1, 10)
    y0 = bvp.exact_unenhanced(x0)

    result = scipy.integrate.solve_bvp(bvp.func, bvp.bc, x0, y0, tol=tol)

    x = np.linspace(0, 1, 100)
    y = result.sol(x)
    u = y[0, :]
    v = y[1, :]
    ju = y[2, 0]
    jv = y[3, 0]

    return x, u, v


def plot_concentrations(x, u, v, axu: plt.Axes, axv: plt.Axes, style={}):
    axu.plot(x, u, color='C0', **style)
    axv.plot(x, v, color='C1', **style)


if __name__ == '__main__':
    main()
