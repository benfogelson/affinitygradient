from figures.pnascommon import *


@click.command()
@click.pass_obj
def main(obj: SumatraContext):
    sumatra_parameters = obj.sumatra_parameters
    data_store = obj.data_store
    proj = obj.proj

    name = 'cytoplasmdependence'

    fig_params = sumatra_parameters[name]

    fig_file_name = '{0}.pdf'.format(name)
    fig_file_path = os.path.join(data_store, fig_file_name)

    fig = plt.figure()
    fig.set_size_inches((linewidth_inches, 1.5*linewidth_inches))
    fig.set_tight_layout(True)

    gs = gridspec.GridSpec(3, 2)
    gs.set_height_ratios([1., 1., .75])

    axu0 = fig.add_subplot(gs[0, 0:2])
    axu0.set_xlabel(r'{\captionfont \(\alpha\) (tether density scale)}')
    axu0.set_ylabel(r'{\captionfont $J_u/J_u^0$}')

    ax_alpha_half = fig.add_subplot(gs[1, 0])

    ax_diffusion_coefficient_analytic = fig.add_subplot(gs[1, 1])

    ax_absolute_flux = fig.add_subplot(gs[2,:])

    records = fig_params['u0_runs']['records'] # type: dict
    sorted_records = sorted(records, key=lambda rec: records[rec]['believed_u0'])
    for r in sorted_records:
        record_label = records[r]['record_label']
        rec = proj.get_record(record_label) # type: sumatra.records.Record

        traj = pypet.Trajectory(name=record_label, add_time=False)
        hdf5filename = rec.parameters['pypet']['hdf5file']
        traj.f_load(load_parameters=2, load_derived_parameters=0, load_results=0, load_other_data=0,
                    filename=hdf5filename)
        traj.v_auto_load = True

        assert traj.u0 == records[r]['believed_u0']

        flux_frame = traj.results.summary.flux_enhancement.flux_frame  # type: pd.DataFrame

        axu0.plot(flux_frame.alpha, flux_frame.enhancement, label=r'\({0}\)'.format(traj.u0))

        plt.sca(axu0)
        plt.legend(title=r'\(\overline{u}\)', loc='upper right')

    u0_title = ''.join([
        r'{\captiontitlefont Flux enhancement for varying cytoplasmic cargo concentrations}'
    ])
    axu0title = axu0.set_title(u0_title)

    asymptotic_record_label = fig_params['asymptotic_run']['record_label']
    traj = pypet.Trajectory(name=asymptotic_record_label, add_time=False)
    traj.f_load(load_parameters=2, load_derived_parameters=0, load_results=0, load_other_data=0,
                filename=hdf5filename)
    traj.v_auto_load = True

    df = traj.results.summary.asymptoticframe.df # type: pd.DataFrame
    ax_alpha_half.loglog(df.u0, df.alpha_half)
    ax_alpha_half.set_xlabel(r'{\captionfont \(\overline{u}\)}')
    ax_alpha_half.set_ylabel(r'{\captionfont $\alpha_{1/2}$}')

    alpha_half_title = ''.join([
        r'{\captiontitlefont ',
        r'Tether density at half}',
        '\n',
        r'{\captiontitlefont maximal flux enhancement',
        r'}'
        ])

    ax_alpha_half_title = ax_alpha_half.set_title(alpha_half_title)

    u = np.linspace(0, 100, 1000)
    u = np.logspace(-2, 2, 1000)

    ax_diffusion_coefficient_analytic.semilogx(u, 1 + u/(1 + u)**2)

    ax_diffusion_coefficient_analytic.set_xlabel(r'{\captionfont \(u\)}')
    ax_diffusion_coefficient_analytic.set_ylabel(
        ''.join([
            r'{\captionfont Effective diffusion}',
            '\n'
            r'{\captionfont coefficient of \(u\)}'
            ])
    )

    diffusion_coefficient_title = ''.join([
        r'{\captiontitlefont Increased diffusion of \(\pmb u\)}',
        '\n',
        r'{\captiontitlefont due to self-competition}'
    ])
    ax_diffusion_coefficient_analytic.set_title(diffusion_coefficient_title)

    absolute_flux_record_label = fig_params['absolute_flux_run']['record_label']
    traj = pypet.Trajectory(name=absolute_flux_record_label, add_time=False)
    traj.f_load(load_parameters=2, load_derived_parameters=0, load_results=0, load_other_data=0,
                filename=hdf5filename)
    traj.v_auto_load = True

    df = traj.results.summary.flux_enhancement.flux_frame # type: pd.DataFrame

    ax_absolute_flux.plot(df.u0, df.ju_unenhanced, label=r'$J_u^0$')
    ax_absolute_flux.plot(df.u0, df.ju, label=r'$J_u$')

    plt.sca(ax_absolute_flux)
    plt.legend()

    absolute_flux_title = ''.join([
        r'{\captiontitlefont Unenhanced (\textcolor{C0}{blue}) and enhanced (\textcolor{C1}{orange}) flux}'
    ])
    ax_absolute_flux.set_title(absolute_flux_title)
    ax_absolute_flux.set_xlabel(r'{\captionfont \(\overline{u}\) (cytoplasmic cargo concentration)}')



    add_letter(axu0, 'a', x_anchor=ax_alpha_half)
    add_letter(ax_alpha_half, 'b')
    add_letter(ax_diffusion_coefficient_analytic, 'c')
    add_letter(ax_absolute_flux, 'd', x_anchor=ax_alpha_half)

    fig.savefig(fig_file_path)

    copy_to_recent_fig_directory(sumatra_parameters, fig_file_path)


if __name__ == '__main__':
    main()