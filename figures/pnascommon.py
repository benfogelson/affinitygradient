import matplotlib
import matplotlib.transforms as transforms
import matplotlib.patches as patches

import matplotlib.pyplot as plt
import matplotlib.lines
from matplotlib import gridspec

import numpy as np
import scipy.integrate
import click
import pypet
import sumatra.parameters
import sumatra.projects
import sumatra.records
import os
import shutil
import pandas as pd
import subprocess
from parameters import ParameterSet

import PyPDF2 as pypdf2

from affinitygradient import AffinityGradientBVP

# http://www.futurile.net/2016/03/14/partial-colouring-text-in-matplotlib-with-latex/
from matplotlib.backends.backend_pgf import FigureCanvasPgf
matplotlib.backend_bases.register_backend('pdf', FigureCanvasPgf)

pgf_with_latex = {
    "pgf.texsystem": "xelatex",     # Use xetex for processing
    "text.usetex": True,            # use LaTeX to write all text
    "axes.labelsize": 7,
    "font.size": 7,
    "legend.fontsize": 7,
    "axes.titlesize": 7,           # Title size when one figure
    "xtick.labelsize": 7,
    "ytick.labelsize": 7,
    "figure.titlesize": 7,         # Overall figure title
    "pgf.rcfonts": False,           # Ignore Matplotlibrc
    "text.latex.unicode": True,
    "pgf.preamble": [
        r'\usepackage{xcolor}',
        r'\definecolor{C0}{HTML}{1F77B4}',
        r'\definecolor{C1}{HTML}{FF7f0E}',
        r'\usepackage{fontspec}',
        r'\usepackage{unicode-math}',
        r'\newcommand{\captionfont}{\normalfont\sffamily\fontsize{7}{9}\selectfont}'
        r'\newcommand{\captiontitlefont}{\normalfont\bfseries\sffamily\fontsize{7}{9}\selectfont}',
        r'\usepackage{amsbsy}'
    ]
}
matplotlib.rcParams.update(pgf_with_latex)

linewidth_points = 246.09686
points_to_inches = 0.0138889
linewidth_inches = linewidth_points*points_to_inches
golden_ratio_factor = 1.0/1.618


def load_from_config(configfile: click.File):
    ctx = SumatraContext(configfile)
    sumatra_parameters = ctx.sumatra_parameters
    data_store = ctx.data_store
    proj = ctx.proj

    return ctx, sumatra_parameters, data_store, proj


def add_letter(ax: plt.Axes, letter, x_position=-.3, y_position=1, x_anchor=None, y_anchor=None):
    x_transform = ax.title.get_transform() if x_anchor is None else x_anchor.title.get_transform()
    y_transform = ax.title.get_transform() if y_anchor is None else y_anchor.title.get_transform()
    transform = transforms.blended_transform_factory(x_transform, y_transform)

    letter_text = r'{\captiontitlefont (%s)}' % letter

    title_lines = ax.get_title().count('\n')
    letter_text += title_lines*'\n'

    text = ax.text(x_position, y_position, letter_text, transform=transform, ha='left')
    return text


class SumatraContext(object):
    def __init__(self, configfile: click.File):
        self.sumatra_parameters = ParameterSet(configfile.name)
        self.sumatra_label = self.sumatra_parameters['sumatra_label']

        self.proj = sumatra.projects.load_project()  # type: sumatra.projects.Project
        self.data_store_root = self.proj.data_store.root
        self.data_store = os.path.join(self.data_store_root, self.sumatra_label)


def save_with_sumatra_metadata(fig: plt.Figure, fig_file_path: str, record_label: str):
    assert fig_file_path.endswith('.pdf')

    fig.savefig(fig_file_path)

    reader = pypdf2.PdfFileReader(fig_file_path)
    writer = pypdf2.PdfFileWriter()

    writer.appendPagesFromReader(reader)

    writer.addMetadata({u'/sumatrarecord': u'{0}'.format(record_label)})

    with open(fig_file_path, 'wb') as f:
        writer.write(f)

def copy_to_recent_fig_directory(sumatra_parameters, fig_file_path: str):
    recent_fig_directory = sumatra_parameters['recent_fig_directory']
    if not os.path.exists(recent_fig_directory):
        os.mkdir(recent_fig_directory)

    shutil.copy(fig_file_path, recent_fig_directory)

    if sumatra_parameters['open_after']:
        subprocess.call(['open', os.path.join(recent_fig_directory, fig_file_path)])