from figures.pnascommon import *


@click.command()
@click.pass_obj
def main(obj: SumatraContext):
    sumatra_parameters = obj.sumatra_parameters
    data_store = obj.data_store
    proj = obj.proj

    name = 'vrates'

    fig_params = sumatra_parameters[name]

    fig_file_name = '{0}.pdf'.format(name)
    fig_file_path = os.path.join(data_store, fig_file_name)

    fig = plt.figure()
    fig.set_size_inches((linewidth_inches, 1.2*linewidth_inches))
    fig.set_tight_layout(True)

    gs = plt.GridSpec(3, 1)
    gs.set_height_ratios([1, 1, 1.618])

    ax_lambdav = fig.add_subplot(gs[0, 0])
    ax_deltav = fig.add_subplot(gs[1, 0])
    ax_contour = fig.add_subplot(gs[2, :])

    for record_label in fig_params['lambdav']['record_labels']:
        record = proj.get_record(record_label)  # type: sumatra.records.Record

        traj = pypet.Trajectory(name=record_label, add_time=False)
        hdf5filename = record.parameters['pypet']['hdf5file']
        traj.f_load(load_parameters=2, load_derived_parameters=0, load_results=0, load_other_data=0,
                    filename=hdf5filename)
        traj.v_auto_load = True

        df = traj.results.summary.flux_enhancement.flux_frame   # type: pd.DataFrame
        # df = df[df.success == True]

        # ax_lambdav.semilogx(df.lambdav, df.enhancement)
        ax_lambdav.loglog(df.lambdav, df.enhancement)

    ax_lambdav.plot(df.lambdav, np.ones_like(df.lambdav), '--', color='0.35', linewidth=1)

    ax_lambdav.set_title(r'{\captiontitlefont Flux of \(\pmb u\) is enhanced by low \(\pmb v\) affinity}')
    ax_lambdav.set_xlabel(r'{\captionfont $\lambda_v$ (chaperone-tether affinity)}')
    ax_lambdav.set_ylabel(r'{\captionfont $J_u/J_u^0$}')

    for record_label in fig_params['deltav']['record_labels']:
        record = proj.get_record(record_label)  # type: sumatra.records.Record

        traj = pypet.Trajectory(name=record_label, add_time=False)
        hdf5filename = record.parameters['pypet']['hdf5file']
        traj.f_load(load_parameters=2, load_derived_parameters=0, load_results=0, load_other_data=0,
                    filename=hdf5filename)
        traj.v_auto_load = True

        df = traj.results.summary.flux_enhancement.flux_frame   # type: pd.DataFrame
        df = df[df.success == True]

        ax_deltav.semilogx(df.deltav, df.enhancement)

    ax_deltav.plot(df.deltav, np.ones_like(df.deltav), '--', color='0.35', linewidth=1)

    ax_deltav.set_title(r'{\captiontitlefont Flux of \(\pmb u\) is enhanced by high \(\pmb v\) diffusivity}')
    ax_deltav.set_xlabel(r'{\captionfont $\delta_v$} (chaperone diffusion coefficient)')
    ax_deltav.set_ylabel(r'{\captionfont $J_u/J_u^0$}')

    record_label = fig_params['contour']['record_label']
    traj = pypet.Trajectory(name=record_label, add_time=False)
    hdf5filename = record.parameters['pypet']['hdf5file']
    traj.f_load(load_parameters=2, load_derived_parameters=0, load_results=0, load_other_data=0,
                filename=hdf5filename)
    traj.v_auto_load = True

    p1 = traj.results.summary.explore2d.p1
    p2 = traj.results.summary.explore2d.p2
    e = traj.results.summary.explore2d.e    # type: np.ndarray
    success = traj.results.summary.explore2d.success


    p1, p2, e, success = map(
        lambda l: np.array(l).reshape(traj.exploration2d.param1.num, traj.exploration2d.param2.num),
        [p1, p2, e, success])

    e_ma = np.ma.masked_array(e, 1 - success)

    cmap = ax_contour.pcolor(p2, p1, e_ma)
    ax_contour.set_xscale('log')
    ax_contour.set_yscale('log')

    ax_contour.contour(p2, p1, e_ma, levels=[1], colors='white', linewidths=2, linestyles='dashed')

    ax_contour.set_title(r'{\captiontitlefont Flux enhancement}')

    # Adapting from https://werthmuller.org/blog/2014/circle/

    # xy = (.1, 10**.5)
    #
    # xr = 1
    # yb = 1
    #
    # xy_display = ax_contour.transData.transform_point(xy)
    # xr_display, _ = ax_contour.transData.transform_point((xr, xy[1]))
    # _, yb_display = ax_contour.transData.transform_point((xy[0], yb))
    #
    # width = xr - xy_display[0]
    # height = xy_display[1] - yb
    #
    # # tscale = ax_contour.transScale + (ax_contour.transLimits + ax_contour.transAxes)
    # # ctscale = tscale.transform_point(xy)
    # # cfig = fig.transFigure.inverted().transform(ctscale)
    #
    # ellipse = patches.Ellipse(xy_display, width, height, transform=None,
    #                           edgecolor='C1', fill=False, linestyle='dashed', linewidth=2)
    #
    # ax_contour.add_artist(ellipse)

    # ax_contour.plot(.1, 2, color='C1', marker='o', markersize=5)
    ax_contour.text(.1, 2, r'{\bfseries X}', fontsize=12, color='C1')

    assert traj.exploration2d.param1.name == 'deltav'
    assert traj.exploration2d.param2.name == 'lambdav'

    ax_contour.set_ylabel(r'{\captionfont $\delta_v$}')
    ax_contour.set_xlabel(r'{\captionfont $\lambda_v$}')

    plt.colorbar(cmap)

    add_letter(ax_lambdav, 'a', -.15, 1)
    add_letter(ax_deltav, 'b', -.15, 1)
    add_letter(ax_contour, 'c', -.15, 1, x_anchor=ax_deltav)

    # fig.savefig(fig_file_path)
    save_with_sumatra_metadata(fig, fig_file_path, sumatra_parameters['sumatra_label'])

    copy_to_recent_fig_directory(sumatra_parameters, fig_file_path)



if __name__ == '__main__':
    main()