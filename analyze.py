import click
import pypet
import sumatra.parameters
import sumatra.projects
import sumatra.records
import os
import pandas as pd
import subprocess
from parameters import ParameterSet

import matplotlib
matplotlib.use('PDF')

from matplotlib import rc
rc('text', usetex=True)
rc('font', **{'serif': 'Computer Modern Roman',
              'sans-serif':'Computer Modern Sans Serif'})

import matplotlib.pyplot as plt


@click.command()
@click.argument('record_label', str)
@click.argument('config_file', str, default='')
def main(config_file, record_label):
    proj = sumatra.projects.load_project() # type: sumatra.projects.Project
    rec = proj.get_record(record_label) # type: sumatra.records.Record

    if config_file != '':
        new_config = ParameterSet(config_file)
        new_record_label = new_config['sumatra_label']
        data_store_root = proj.data_store.root
        click.echo('Data_store_root = {0}'.format(data_store_root), color='red')
        data_store = os.path.join(data_store_root, new_record_label)
        if not os.path.exists(data_store):
            os.makedirs(data_store)
    else:
        new_record_label = 'out'
        data_store = '.'

    par = rec.parameters    # type: sumatra.parameters.ParameterSet

    hdf5filename = par['pypet']['hdf5file']
    assert os.path.exists(hdf5filename)

    traj = pypet.Trajectory(name=record_label, add_time=False)
    traj.f_load(load_parameters=2, load_derived_parameters=0, load_results=0, load_other_data=0, filename=hdf5filename)
    traj.v_auto_load = True

    flux_frame = traj.results.summary.flux_enhancement.flux_frame   # type: pd.DataFrame

    fig = plt.figure()
    fig.set_size_inches(6,4)
    ax = fig.add_subplot(111)
    if traj.exploration.spacing == 'linear':
        ax.plot(flux_frame[traj.exploration.param], flux_frame.enhancement)
    elif traj.exploration.spacing == 'log':
        ax.semilogx(flux_frame[traj.exploration.param], flux_frame.enhancement)
    # ax.set_xlabel(r'\(\alpha\)', size=14)
    ax.set_ylabel(r'\(\frac{j_u}{j_u^0}\)', rotation='horizontal', size=14)
    fig.set_tight_layout(True)

    # title = r', '.join([
    #     r'\(u_0 = {0}'.format(par['model']['u0']),
    #     r'\delta_v = {0}'.format(par['model']['deltav']),
    #     r'\kappa_u^{{out}} = {0}'.format(par['model']['kappaoutu']),
    #     r'\kappa_v^{{out}} = {0}'.format(par['model']['kappaoutv']),
    #     r'\lambda_u = {0}'.format(par['model']['lambdau']),
    #     r'\lambda_v = {0}\)'.format(par['model']['lambdav'])
    # ])
    # title = r'\noindent Data: {0} \em \em Plot: {1}\\\\'.format(record_label, new_record_label) + title
    # ax.set_title(title)

    fig_file_name = os.path.join(data_store, '%s.pdf' % new_record_label)
    fig.savefig(fig_file_name)

    subprocess.call(['open', fig_file_name])

if __name__ == '__main__':
    main()