This repository provides application code for solving the model equations
for competitive transport through the nuclear pore, as described in

Fogelson, B., & Keener, J. P. (2018). Enhanced Nucleocytoplasmic Transport due to Competition for Elastic Binding Sites.
Biophysical Journal, 115(1), 108–116. https://doi.org/10.1016/j.bpj.2018.05.034
